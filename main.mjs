import { MacroCharacter } from "./scripts/character.js";
import { MacroSheet } from "./scripts/sheet.js";
import { MacroChat } from "./scripts/chat.js";
import { MacroPlayers } from "./scripts/players.js";
import { MacroCombat } from "./scripts/combat.js";

export class Tor1eMacros {
    constructor() {
        this.characters = new MacroCharacter();
        this.sheet = new MacroSheet();
        this.chat = new MacroChat();
        this.players = new MacroPlayers();
        this.combat = new MacroCombat();

        this.TOR1E_MACROS_DEBUG = false;
    }

    // ==========================================
    // CHARACTERS
    // ==========================================
    // Characters List of advancement points per skill group
    async characters_List_Advancement_Points() {
        this.characters.characters_List_Advancement_Points();
    }
    // Characters Physical health
    async characters_List_Health_Physical() {
        this.characters.characters_List_Health_Physical();
    }
    // Characters Moral health
    async characters_List_Health_Moral() {
        this.characters.characters_List_Health_Moral();
    }
    // Characters Valour & Wisdom 
    async characters_List_Valour_Wisdom() {
        this.characters.characters_List_Valour_Wisdom();
    }
    // PJ sélectionné : Remettre la santé physique au max
    async characters_Set_EtatPhysiqueMax() {
        this.characters.characters_Set_EtatPhysiqueMax();
    }  
    // Characters : Synthesis of experience and calculation of Courage points
    async characters_List_Experience_Courage() {
        this.characters.characters_List_Experience_Courage();
    }
    // Characters : Add Advancement and Experience points
    async characters_Add_SessionProgression() {
        this.characters.characters_Add_SessionProgression();
    }    
    
    // ==========================================
    // COMBAT
    // ==========================================
    // Combat : Launch an attack for an Adversary
    async combat_lanceAttaque() {
        this.combat.combat_lanceAttaque();
    }
    
    // ==========================================
    // TOKENS
    // ==========================================
    // Selected token(s) : set Dead or not over the token 
    async tokens_Set_DeadOrNot() {
        this.sheet.tokens_Set_DeadOrNot();
    }
    // Selected token(s) or tile(s) : hide or not
    async objects_Set_Hidden_or_not() {
        this.sheet.objects_Set_Hidden_or_not();
    }

    // Selected token(s) : Dead or not as small icon in top left of the token
    async tokens_Set_EtatMort() {
        this.sheet.tokens_Set_EtatMort();
    }
    // Selected token(s) : Injured or not as small icon in top left of the token
    async tokens_Set_EtatBlesse() {
        this.sheet.tokens_Set_EtatBlesse();
    }
    // Selected token(s) : Weary or not as small icon in top left of the token
    async tokens_Set_EtatEpuise() {
        this.sheet.tokens_Set_EtatEpuise();
    }   
    // Selected token(s) : Inconscious or not as small icon in top left of the token
    async tokens_Set_EtatInconscient() {
        this.sheet.tokens_Set_EtatInconscient();
    }   

    // Selected token(s) : Change size
    async tokens_Set_Size() {
        this.sheet.tokens_Set_Size();
    }

    // ==========================================
    // SHEET
    // ==========================================
    // Sheet display
    async sheet_DisplaySheet() {
        this.sheet.sheet_DisplaySheet();
    }
    // Token image display
    async sheet_DisplayTokenImage() {
        this.sheet.sheet_DisplayTokenImage();
    }

    // ==========================================
    // CHAT
    // ==========================================
    // Send a message in the chat
    async chat_SendMessage() {
        this.chat.sendMessage();
    }
    
    // Send a message in a bubble of the player character or of a token selected by the GM
    async chat_SendMessageBuble() {
        this.chat.sendMessageBuble();
    }

    // ==========================================
    // PLAYERS
    // ==========================================
    // List the connected players
    async players_ListConnectedPlayers() {
        this.players.listConnectedPlayers();
    }
}

Hooks.once('init', async function () {
    game.tor1eMacros = new Tor1eMacros();
    console.log("=== Tor1eMacros : Init ok===");
});