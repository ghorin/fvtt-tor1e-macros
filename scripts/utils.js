export class MacroUtil {
    constructor(_perimetre) {
        this.perimetre = _perimetre;
    }

    getTraduction(_termeOriginal) {
        let _termeTraduit = game.i18n.localize("fvtt-tor1e-macros." + this.perimetre + "." + _termeOriginal + "");
        return _termeTraduit;
    }
}