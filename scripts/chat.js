import { MacroUtil } from "./utils.js";

export class MacroChat {

    constructor() {
    }

    sendMessage() {
        // ==================================================
        //    MESSAGE SENT TO THE CHAT
        // ==================================================
        // Get traduction of terms
        let macroUtil = new MacroUtil("chat_sendMessage");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogMessage = macroUtil.getTraduction("message");
        let trad_DialogDestinataires = macroUtil.getTraduction("destinataires");
        let trad_DialogGardienDesLegendes = macroUtil.getTraduction("gardiendeslegendes");
        let trad_DialogTouslesjoueurs = macroUtil.getTraduction("touslesjoueurs");
        let trad_DialogContinuer = macroUtil.getTraduction("continuer");
        let trad_DialogTouches = macroUtil.getTraduction("touches");
        

        let formHtml = "<table><tr><th nowrap><label>"+trad_DialogMessage+" : </label></th><td><textarea name='texte-message' rows='15' style='width: 500px'></textarea></td></tr>";
        formHtml = formHtml + "<tr><th nowrap><label>"+trad_DialogDestinataires+" : </label></th><td><select id='dest-message' multiple rows=10 style='width: 300px'>";
        formHtml = formHtml + "<option value='tous'>"+trad_DialogTouslesjoueurs+"</option>";
        formHtml = formHtml + "<option value='gm'>"+trad_DialogGardienDesLegendes+"</option>";
        game.users.forEach(t => {
            if (t.active == true && t.character != null && !t.isGM) {
                let perso = t.character;
                if (perso != null) {
                    if (perso.type == "character") {
                        formHtml = formHtml + "<option value='" + t.name + "'>" + perso.name + " (" + t.name + ")</option>";
                    }
                }
            }
        });
        formHtml = formHtml + "</select></td></tr><tr><td></td><td>Use CTRL for selecting multiple targets for your message.</td></tr></table>";
        new Dialog({
            title: trad_DialogTitre,
            content: formHtml,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: trad_DialogContinuer
                }
            },
            default: 'yes',
            close: html => {
                let monMessage = html.find('[name=texte-message]')[0]?.value || null;
                if (monMessage != null) {
                    let cible = html.find('select#dest-message');
                    var options = cible[0].selectedOptions;
                    var values = Array.from(options).map(({ value }) => value);

                    let message = {
                        content: monMessage
                    };

                    if (cible == "tous") {
                        ChatMessage.create(message);
                    } else {
                        for (var i = 0; i < values.length; i++) {
                            message.whisper = ChatMessage.getWhisperRecipients(values[i]);
                            ChatMessage.create(message);
                        }
                    }
                }
            }
        },{width:600, height:600}).render(true);
    }

    sendMessageBuble() {
        // Get traduction of terms
        let macroUtil = new MacroUtil("chat_sendMessageBuble");

        let trad_DialogAlertee = macroUtil.getTraduction("alerte_select_token");
        let trad_DialogTitre = macroUtil.getTraduction("alerte_select_token");
        let trad_DialogMessage = macroUtil.getTraduction("message");
        let trad_DialogContinuer = macroUtil.getTraduction("continuer");


        let monActor = null;
        const isGM = game.users.get(game.userId).hasRole(4);
        if (isGM) {
            let tokens = canvas.tokens.controlled;
            if (tokens != null) {
                monActor = tokens[0];
            }
        } else {
            monActor = game.user.character;
        }
        if (monActor == null) {
            ui.notifications.notify(trad_DialogAlertee);
        } else {
            let formHtml = "<label>"+trad_DialogMessage+" : </label><textarea name='texte-message' rows='9' style='width: 380px'></textarea>";
            new Dialog({
                title: trad_DialogTitre,
                content: formHtml,
                buttons: {
                    yes: {
                        icon: "<i class='fas fa-check'></i>",
                        label: trad_DialogContinuer
                    }
                },
                default: 'yes',
                close: html => {
                    let monMessage = html.find('[name=texte-message]')[0]?.value || null;
                    if (monMessage != null) {
                        let speaker = ChatMessage.getSpeaker({ monActor });

                        ChatMessage.create({
                            speaker: speaker,
                            content: monMessage,
                            type: CONST.CHAT_MESSAGE_TYPES.EMOTE
                        }, { chatBubble: true });
                    }
                }
            }).render(true);
        }
    }
}
