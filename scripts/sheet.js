import { MacroUtil } from "./utils.js";

export class MacroSheet {

    constructor() {
    }

    // ===========================================================================================
    //           EFFECT ICONS ON THE TOKEN
    // ===========================================================================================
    // DEAD (mort)
    tokens_Set_EtatMort() {
        const effect = "modules/fvtt-tor1e-macros/icons/etat-mort.webp";
        canvas.tokens.controlled.forEach(token => {
            token.toggleEffect(effect);
        });
    }
    // INJURED (blessé)
    tokens_Set_EtatBlesse() {
        const effect = "modules/fvtt-tor1e-macros/icons/etat-blesse.webp";
        canvas.tokens.controlled.forEach(token => {
            token.toggleEffect(effect);
        });
    }
    // WEARY (épuisé)
    tokens_Set_EtatEpuise() {
        const effect = "modules/fvtt-tor1e-macros/icons/etat-epuise.webp";
        canvas.tokens.controlled.forEach(token => {
            token.toggleEffect(effect);
        });
    }
    // INCONSCIOUS / OUT-OF-COMBAT (inconscient / hors de combat)
    tokens_Set_EtatInconscient() {
        const effect = "modules/fvtt-tor1e-macros/icons/etat-inconscient.webp";
        canvas.tokens.controlled.forEach(token => {
            token.toggleEffect(effect);
        });
    }



    // ===========================================================================================
    //           OVERLAY ICON ON THE TOKEN
    // ===========================================================================================
    // DEAD (mort)
    tokens_Set_DeadOrNot() {
        const icone = "modules/fvtt-tor1e-macros/icons/etat-mort.webp";
        canvas.tokens.controlled.forEach(token => {
            token.toggleEffect(icone, { overlay: true });
        });
    }
    
    // ===========================================================================================
    //           HIDDEN OR SHOWED
    // ===========================================================================================
    // Show or hide the selected token(s) or tile(s) in the current canvas
    objects_Set_Hidden_or_not() {
        // Tokens
        const updateTokens = [];
        for (let token of canvas.tokens.controlled) {
            updateTokens.push({
                _id: token.id,
                hidden: !token.document.hidden
            });
        };
        canvas.scene.updateEmbeddedDocuments("Token", updateTokens);

        // Tiles
        const updateTiles = [];
        for (let tile of canvas.tiles.controlled) {
            updateTiles.push({
               _id: tile.id,
               hidden: !tile.document.hidden
            });
        };
        canvas.scene.updateEmbeddedDocuments("Tile", updateTiles);
    }

    // ===========================================================================================
    //           CHANGE THE TOKEN SIZE ON CURRENT SCENE
    // ===========================================================================================
    tokens_Set_Size() {
        // Get traduction of terms
        let macroUtil = new MacroUtil("tokens_Set_Size");
        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogFermer = macroUtil.getTraduction("fermer");
        let trad_selectToken = macroUtil.getTraduction("alerte_select_token");

        let tokens = canvas.tokens.controlled;
        if (tokens.length == 0) {
            ui.notifications.warn(trad_selectToken);
        } else {
            const transformations = {
                microscopique: {
                    label: "0.1",
                    scale: 0.1
                },
                minuscule: {
                    label: "0.25",
                    scale: 0.25
                },
                petit: {
                    label: "0.5",
                    scale: 0.5
                },
                moyen: {
                    label: "0.75",
                    scale: 0.75
                },
                normal: {
                    label: "1",
                    scale: 1
                },
                grand: {
                    label: "1.5",
                    scale: 1.5
                },
                geant: {
                    label: "2",
                    scale: 2
                },
                gigantesque: {
                    label: "3",
                    scale: 3
                },
                immense: {
                    label: "5",
                    scale: 5
                },
                superimmense: {
                    label: "7",
                    scale: 7
                },
                megaimmense: {
                    label: "9",
                    scale: 9
                }, 
            };
    
    
            new Dialog({
                title: trad_DialogTitre ,
                content: `
                    <form>
                        <div style="display: flex; align-content: center;">
                        </div>
                    </form>`,
                buttons: this.getTransformations(transformations),
                default: trad_DialogFermer ,
                close: () => { }
            }).render(true);
        }
    }
    getTransformations(transformations) {
        let transformButtons = {};
        Object.entries(transformations).forEach(([key, transform]) => {
            transformButtons[key] = {
                label: transform.label,
                callback: (html) => {
                    this.redimToken(transform.scale);
                }
            }
        });
        return transformButtons;
    }
    redimToken(facteur) {
        const update = {
            scale: facteur
        };

        const updates = [];
        for (let token of canvas.tokens.controlled) {
          updates.push({
            _id: token.id,
            scale: facteur
          });
        };
        canvas.scene.updateEmbeddedDocuments("Token", updates);        
    }    


    // ===========================================================================================
    //           DISPLAY THE CHARACTER SHEET
    // ===========================================================================================
    sheet_DisplaySheet() {
        // ===========================================================================================
        // Display the character sheet (as a player) or the sheet of the selected token (GM)
        // ===========================================================================================
        const isGM = game.users.get(game.userId).hasRole(4);
        if (isGM) {
            let tokens = canvas.tokens.controlled;
            let token = null;
            if (tokens.length > 0) {
                token = tokens[0];
            }
            if (token == null) {
                ui.notifications.warn("Please select token first.");
            } else {
                token.actor.sheet.render(true);
            }
        } else {
            game.user.character.sheet.render(true);
        }
    }

    // ===========================================================================================
    // Display to all players the token image in a large view
    // ===========================================================================================
    sheet_DisplayTokenImage() {
        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn("Please select token first.");
        } else {
            let tActor = token.actor;
            let ip = new ImagePopout(tActor.img, {
                title: tActor.name,
                shareable: true,
                uuid: tActor.uuid
            }).render(true);
            ip.shareImage();
        }
    }
}