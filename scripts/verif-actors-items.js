let nbActorFailed = 0;
let nbItemsFailed = 0;
let nbItemsOwned = 0;
let failed = false;

game.actors.forEach(t => {
    failed = false;
    // Le personnage a t'il une image ?
    if(t.img === null || t.img === undefined || t.img.includes("mystery-man.svg") ) {
        console.log(t.name + " => No image for the character");
        failed = true;
    }
    // Le personnage a t'il un token ?
    if(t.data.token === null || t.data.token === undefined) {
        console.log(t.name + " => no token");
        failed = true;
    } else {
        // Le token a t'il une image ?
        if(t.data.token.img === null || t.data.token.img === undefined || t.data.token.img.includes("mystery-man.svg") ) {
            console.log(t.name + " => No image for the token");
            failed = true;
        }
    }
    t.items.forEach(i => {
        nbItemsOwned++;
        // L'Owned item a t'il une image ?
        if(i.img === null || i.img === undefined) {
            console.log(t.name + " : " + i.name + " => No image for the embedded item of the character");
            failed = true;
        }
    });
    if(failed === true) {
        nbActorFailed++;
    }
});
game.items.forEach(t => {
    // L'item a t'il une image ?
    if(t.img === null || t.img === undefined) {
        console.log(t.name + " :" + t.img);
        nbItemsFailed++;
    }
    if(t.isOwned == true) {
        console.log("owned");
    }
});

console.log("============");
console.log("- "+game.actors.size+" actors with " + nbItemsOwned + " items.");
console.log("  with "+nbActorFailed+" KO");
console.log("- "+game.items.size+" items");
console.log("  with "+nbItemsFailed+" KO");
console.log("============");
