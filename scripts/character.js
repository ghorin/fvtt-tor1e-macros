import { MacroUtil } from "./utils.js";


export class MacroCharacter {

    constructor() {
    }


    async characters_List_Advancement_Points() {
        console.log("=============== POINTS D'AVANCEMENT PAR GROUPE =================")

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Advancement_Points");
        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogQuestion = macroUtil.getTraduction("question");
        
        let trad_titreTableau = macroUtil.getTraduction("titreTableau");
        let trad_groupePersonnalite = macroUtil.getTraduction("groupePersonnalite");
        let trad_groupeDeplacement = macroUtil.getTraduction("groupeDeplacement");
        let trad_groupePerception = macroUtil.getTraduction("groupePerception");
        let trad_groupeSurvie = macroUtil.getTraduction("groupeSurvie");
        let trad_groupeCoutume = macroUtil.getTraduction("groupeCoutume");
        let trad_groupeMetier = macroUtil.getTraduction("groupeMetier");


        // GET THE LIST OF ACTORS TO TAKE IN ACCOUNT
        let listeActors = [];
        var cpt = 0;
        var valeur_nulle = "-";

        let confirmation = await Dialog.confirm({
            title: trad_DialogTitre,
            content: "<p>" + trad_DialogQuestion + "</p>"
        });
        if (confirmation == true) {
            // GET ALL THE CHARACTER ACTORS
            game.actors.forEach(a => {
                if (a.type == "character") {
                    listeActors[cpt] = a;
                    cpt = cpt + 1;
                }
            });
        } else {
            // GET ONLY THE CHARACTERS OF CONNECTED PLAYERS
            game.users.forEach(u => {
                if (u.active == true && !u.isGM) {
                    let p = game.actors.get(u.character);
                    if (p != null) {
                        if (p.type == "character") {
                            listeActors[cpt] = p;
                            cpt = cpt + 1;
                        }
                    }
                }
            });
        }


        // LOOP ON ALL ACTORS : GET THE Advancement Points
        console.log("==> List of the characters and Advancement Points per Group");

        // START HTML TEXT
        let message = {
                content: "<table><tr><th colspan=3 style='text-align: center; color: darkred;'>=== " + trad_titreTableau + " ===</th></tr>"
        };


        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            let groupePersonnalite = perso.system.skillGroups.personality.value;
            if(groupePersonnalite == 0) groupePersonnalite = valeur_nulle;

            let groupeDeplacement = perso.system.skillGroups.movement.value;
            if(groupeDeplacement == 0) groupeDeplacement = valeur_nulle;

            let groupePerception = perso.system.skillGroups.perception.value;
            if(groupePerception == 0) groupePerception = valeur_nulle;

            let groupeSurvie = perso.system.skillGroups.survival.value;
            if(groupeSurvie == 0) groupeSurvie = valeur_nulle;

            let groupeCoutume = perso.system.skillGroups.custom.value;
            if(groupeCoutume == 0) groupeCoutume = valeur_nulle;

            let groupeMetier = perso.system.skillGroups.vocation.value;
            if(groupeMetier == 0) groupeMetier = valeur_nulle;
            
            message.content = message.content + "<tr><th style='text-align: left; color: darkred;' colspan=3>" + perso.name + "</th></tr>";
            message.content = message.content + "<tr><th>&nbsp;&nbsp;&nbsp;</th><td>" + trad_groupePersonnalite + "</td><td>" + groupePersonnalite + "</td></tr>";
            message.content = message.content + "<tr><th>&nbsp;&nbsp;&nbsp;</th><td>" + trad_groupeDeplacement + "</td><td>" + groupeDeplacement + "</td></tr>";
            message.content = message.content + "<tr><th>&nbsp;&nbsp;&nbsp;</th><td>" + trad_groupePerception + "</td><td>" + groupePerception + "</td></tr>";
            message.content = message.content + "<tr><th>&nbsp;&nbsp;&nbsp;</th><td>" + trad_groupeSurvie + "</td><td>" + groupeSurvie + "</td></tr>";
            message.content = message.content + "<tr><th>&nbsp;&nbsp;&nbsp;</th><td>" + trad_groupeCoutume + "</td><td>" + groupeCoutume + "</td></tr>";
            message.content = message.content + "<tr><th>&nbsp;&nbsp;&nbsp;</th><td>" + trad_groupeMetier + "</td><td>" + groupeMetier + "</td></tr>";

        }

        message.content = message.content + "</table>";


        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }


    // ===================================================
    // SYNTHESE DE L'EXPERIENCE ET CALCUL DES POINTS DE COURAGE
    // ==================================================
    async characters_List_Experience_Courage() {
        console.log("=============== POINTS OF COURAGE =================")

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Experience_Courage");
        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogQuestion = macroUtil.getTraduction("question");
        let trad_TableExperienceCourage = macroUtil.getTraduction("ExperienceCourage");
        let trad_TableExperience = macroUtil.getTraduction("Experience");
        let trad_TableCourage = macroUtil.getTraduction("Courage");



        // GET THE LIST OF ACTORS TO TAKE IN ACCOUNT
        let listeActors = [];
        var cpt = 0;

        let confirmation = await Dialog.confirm({
            title: trad_DialogTitre,
            content: "<p>" + trad_DialogQuestion + "</p>"
        });
        if (confirmation == true) {
            // GET ALL THE CHARACTER ACTORS
            game.actors.forEach(a => {
                if (a.type == "character") {
                    listeActors[cpt] = a;
                    cpt = cpt + 1;
                }
            });
        } else {
            // GET ONLY THE CHARACTERS OF CONNECTED PLAYERS
            game.users.forEach(u => {
                if (u.active == true && !u.isGM) {
                    let p = game.actors.get(u.character);
                    if (p != null) {
                        if (p.type == "character") {
                            listeActors[cpt] = p;
                            cpt = cpt + 1;
                        }
                    }
                }
            });
        }


        // LOOP ON ALL ACTORS : GET THE MAX EXPERIENCE
        console.log("==> List of the characters and total experience")
        let experienceMax = 0;
        let persoMax = null;

        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];
            if( i == 0 ) {
                persoMax = perso;
            }            
            let experiencePerso = perso.system.stature.experience.total.value;
            if (experiencePerso > experienceMax) {
                experienceMax = experiencePerso;
                persoMax = perso;
            }
        }
        console.log("==> Experience Max = " + experienceMax + " (" + persoMax.name + ")");



        // START HTML TEXT
        let message = {
            content: "<table><tr><th colspan=3 style='text-align:center'>" + trad_TableExperienceCourage + "</th></tr><tr><th></th><th style='text-align:center'>" + trad_TableExperience + "</th><th style='text-align:center'>" + trad_TableCourage + "</th></tr>"
        };

        console.log("==> List of the characters and calculated points of Courage")
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];
            let expPerso = perso.system.stature.experience.total.value;
            let ptsCourage = Math.trunc((experienceMax - expPerso) / 10);

            message.content = message.content + "<tr><th>" + perso.name + "</th>";
            message.content = message.content + "<td style='text-align:center'>" + expPerso + "</td>";
            if (ptsCourage == 0) {
                message.content = message.content + "<th style='text-align:center'>0</th>";
            } else {
                message.content = message.content + "<th style='text-align:center; color:darkred'>" + ptsCourage + "</th>";
            }
            message.content = message.content + "</tr>";
        }
        // END OF THE HTML TEXT
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }


    // ===================================================
    // SYNTHESE DE LA SANTE PHYSIQUE DES PJ
    // ==================================================
    async characters_List_Health_Physical() {
        // ===================================================
        // - For each character, display its Endurance, total Fatigue, Weary or not, Injured ot not
        // ==================================================
        let idEtat_Epuise = "tor1e.effects.weary";
        let idEtat_Blesse = "tor1e.effects.wounded";
        let idEtat_TempEpuise = "tor1e.effects.temporary-weary";
        let idEtat_TempBlesse = "tor1e.effects.convalescent";
        let idEtat_Empoisonne = "tor1e.effects.poisoned";        

        var cpt = 0;
        let listeActors = [];

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Health_Physical");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogQuestion = macroUtil.getTraduction("question");

        let trad_TableSantePhysique = macroUtil.getTraduction("sante_physique");
        let trad_TableEndurance = macroUtil.getTraduction("endurance");
        let trad_TableFatigue = macroUtil.getTraduction("fatigue");
        let trad_TableEtat = macroUtil.getTraduction("etat");

        let trad_Epuise = macroUtil.getTraduction("epuise");
        let trad_Blesse = macroUtil.getTraduction("blesse");
        let trad_Empoisonne = macroUtil.getTraduction("empoisonne");
        let trad_Bon = macroUtil.getTraduction("bon");


        // Dialog to ask the perimeter of the synthesis
        let confirmation = await Dialog.confirm({
            title: trad_DialogTitre,
            content: "<p>" + trad_DialogQuestion + "</p>"
        });

        // GET THE LIST OF ACTORS TO TAKE IN ACCOUNT
        if (confirmation == true) {
            // GET ALL THE CHARACTER ACTORS
            game.actors.forEach(a => {
                if (a.type == "character") {
                    listeActors[cpt] = a;
                    cpt = cpt + 1;
                }
            });
        } else {
            // GET ONLY THE CHARACTERS OF CONNECTED PLAYERS
            game.users.forEach(u => {
                if (u.active == true && !u.isGM) {
                    let p = game.actors.get(u.character);
                    if (p != null) {
                        if (p.type == "character") {
                            listeActors[cpt] = p;
                            cpt = cpt + 1;
                        }
                    }
                }
            });
        }


        // START HTML TEXT
        let message = {
            content: "<table><tr><th colspan=3 style='text-align:center'>" + trad_TableSantePhysique + "</th></tr><tr><th></th><th style='text-align:left'>" + trad_TableEndurance + "</th><th style='text-align:center'>" + trad_TableFatigue + "</th></tr>"
        };

        // LOOP ON ALL ACTORS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            // ENDURANCE
            let endurance = perso.system.resources.endurance.value;
            let enduranceMax = perso.system.resources.endurance.max;

            // FATIGUE
            let fatigueVoyage = perso.system.resources.travelFatigue.value;
            let fatigueEnc = 0;
            perso.items.forEach(i => {
                if (i.type == "miscellaneous" || i.type == "weapon" || i.type == "armour") {
                    fatigueEnc = fatigueEnc + i.system.encumbrance.value;
                }
            });
            let fatigueTotale = fatigueVoyage + fatigueEnc;

            // ETAT DE SANTE
            let etatEpuise = 0;
            let etatBlessure = 0;
            let etatEmpoisonne = 0;

            perso.effects.forEach(e => {
                if (e.label == idEtat_Epuise) {
                    etatEpuise = 1;
                } else if (e.label == idEtat_TempEpuise) {
                    etatEpuise = 1;
                } else if (e.label == idEtat_Blesse) {
                    etatBlessure = 1;
                } else if (e.label == idEtat_TempBlesse) {
                    etatBlessure = 1;
                } else if (e.label == idEtat_Empoisonne) {
                    etatEmpoisonne = 1;
                }
            });

            let etatGeneral = "";
            if (etatEpuise != 0) etatGeneral = trad_Epuise;
            if (etatBlessure != 0) {
                if (etatGeneral != "") etatGeneral = etatGeneral + ", ";
                etatGeneral = etatGeneral + trad_Blesse;
            }
            if (etatEmpoisonne != 0) {
                if (etatGeneral != "") etatGeneral = etatGeneral + ", ";
                etatGeneral = etatGeneral + trad_Empoisonne;
            }
            if (etatGeneral == "") etatGeneral = trad_Bon;


            // STYLE D'AFFICHAGE DU RESULTAT
            let styleEndurance = "text-align:left";
            let styleFatigue = "text-align:center";
            let styleEtatGeneral = "text-align:left";

            if (endurance < enduranceMax) {
                styleEndurance = "text-align:left; color:darkred; font-weight: bold;";
            }
            if (fatigueTotale > endurance) {
                styleFatigue = "text-align:center; color:darkred; font-weight: bold;";
            }
            if (etatGeneral != trad_Bon) {
                styleEtatGeneral = "text-align:left; color:darkred";
            }

            // AFFICHER LE RESULTAT
            message.content = message.content + "<tr><th style='text-align:left'>" + perso.name + "</th>";
            message.content = message.content + "<td><span style='" + styleEndurance + "'>" + endurance + "</span> / " + enduranceMax + "</td>";
            message.content = message.content + "<td style='" + styleFatigue +"'>" + fatigueTotale + "</td>";
            message.content = message.content + "</tr><tr><th></th>";
            message.content = message.content + "<td style='" + styleEtatGeneral + "' colspan=2>" + etatGeneral + "</td>";
            message.content = message.content + "</tr>";
        }
        // END OF THE HTML TEXT
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }

    // ==================================================
    // SYNTHESE DE LA SANTE PSYCHOLOGIQUE DES PJ
    // ==================================================
    async characters_List_Health_Moral() {
        // ==================================================
        // - Pour chaque PJ : Espoir, Ombre totale, état Mélanquolique
        // ==================================================
        let idEtat_Melancolique = "tor1e.effects.miserable";
        let idEtat_TempMelancolique = "tor1e.effects.temporary-miserable";

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Health_Moral");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogQuestion = macroUtil.getTraduction("question");

        let trad_TableEspoirOmbre = macroUtil.getTraduction("espoir_ombre");
        let trad_TableEspoir = macroUtil.getTraduction("espoir");
        let trad_TableOmbre = macroUtil.getTraduction("ombre");
        let trad_TableEtat = macroUtil.getTraduction("etat");

        let trad_EtatMelancolique = macroUtil.getTraduction("melancolique");
        let trad_EtatBon = macroUtil.getTraduction("bon");

        var cpt = 0;
        let listeActors = [];

        let confirmation = await Dialog.confirm({
            title: trad_DialogTitre,
            content: "<p>" + trad_DialogQuestion + "</p>"
        });

        // GET THE LIST OF ACTORS TO TAKE IN ACCOUNT
        if (confirmation == true) {
            // GET ALL THE CHARACTER ACTORS
            game.actors.forEach(a => {
                if (a.type == "character") {
                    listeActors[cpt] = a;
                    cpt = cpt + 1;
                }
            });
        } else {
            // GET ONLY THE CHARACTERS OF CONNECTED PLAYERS
            game.users.forEach(u => {
                if (u.active == true && !u.isGM) {
                    let p = game.actors.get(u.character);
                    if (p != null) {
                        if (p.type == "character") {
                            listeActors[cpt] = p;
                            cpt = cpt + 1;
                        }
                    }
                }
            });
        }

        // START HTML TEXT
        let message = {
            content: "<table><tr><th colspan=4 style='text-align:center'>" + trad_TableEspoirOmbre + "</th></tr><tr><th></th><th style='text-align:left'>" + trad_TableEspoir + "</th><th style='text-align:center'>" + trad_TableOmbre + "</th><th style='text-align:left'>" + trad_TableEtat + "</th></tr>"
        };

        // LOOP ON ALL ACTORS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            let espoir = perso.system.resources.hope.value;
            let espoirMax = perso.system.resources.hope.max;
            let ombre = perso.system.resources.shadow.temporary.value + perso.system.resources.shadow.permanent.value;
            let etatMelancolique = 0;

            perso.effects.forEach(e => {
                if (e.label == idEtat_Melancolique) {
                    etatMelancolique = 1;
                } else if (e.label == idEtat_TempMelancolique) {
                    etatMelancolique = 1;
                }
            });

            // STYLE D'AFFICHAGE DU RESULTAT
            let styleEspoir = "text-align:left";
            let styleOmbre = "text-align:center";
            let styleEtatGeneral = "text-align:left";

            if (ombre > espoir) {
                styleOmbre = "text-align:center; color:darkred; font-weight:bold;";
            }
            if (etatMelancolique != 0) {
                styleEtatGeneral = "text-align:left; color:darkred";
            }
            

            message.content = message.content + "<tr><th>" + perso.name + "</th>";
            message.content = message.content + "<td style='"+styleEspoir+"'>" + espoir + " / " + espoirMax + "</td>";
            message.content = message.content + "<td style='"+styleOmbre+"'>" + ombre + "</td>";
            if (etatMelancolique != 0) {
                message.content = message.content + "<td style='"+styleEtatGeneral+"'>" + trad_EtatMelancolique + "</td>";
            } else {
                message.content = message.content + "<td style='"+styleEtatGeneral+"'>" + trad_EtatBon + "</td>";
            }
            message.content = message.content + "</tr>";
        }

        // END OF THE HTML TEXT
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }

    // ==================================================
    // SYNTHESE DE LA VAILLANCE & SAGESSE DES PJ
    // ==================================================
    async characters_List_Valour_Wisdom() {
        // ==================================================
        // - score de chaque PJ
        // - score de Vaillance le plus élevé (et qui)
        // - score de Sagesse le plus élevé (et qui)
        // ==================================================

        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_List_Valour_Wisdom");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogQuestion = macroUtil.getTraduction("question");

        let trad_TableVaillanceSagesse = macroUtil.getTraduction("vaillance_sagesse");
        let trad_TableVaillance = macroUtil.getTraduction("vaillance");
        let trad_TableSagesse = macroUtil.getTraduction("sagesse");



        var cpt = 0;
        let listeActors = [];
        let vaillanceMaxValue = 0;
        let sagesseMaxValue = 0;

        let confirmation = await Dialog.confirm({
            title: trad_DialogTitre,
            content: "<p>" + trad_DialogQuestion + "</p>"
        });

        // GET THE LIST OF ACTORS TO TAKE IN ACCOUNT
        if (confirmation == true) {
            // GET ALL THE CHARACTER ACTORS
            game.actors.forEach(a => {
                if (a.type == "character") {
                    listeActors[cpt] = a;
                    cpt = cpt + 1;
                }
            });
        } else {
            // GET ONLY THE CHARACTERS OF CONNECTED PLAYERS
            game.users.forEach(u => {
                if (u.active == true && !u.isGM) {
                    let p = game.actors.get(u.character);
                    if (p != null) {
                        if (p.type == "character") {
                            listeActors[cpt] = p;
                            cpt = cpt + 1;
                        }
                    }
                }
            });
        }

        // START HTML TEXT
        let message = {
            content: "<table><tr><th colspan=3 style='text-align:center'>" + trad_TableVaillanceSagesse + "</th></tr><tr><th></th><th style='text-align:center'>" + trad_TableVaillance + "</th><th style='text-align:center'>" + trad_TableSagesse + "</th></tr>",
            whisper: game.users.filter(u => u.isGM).map(u => u._id)
        };

        // LOOP ON ALL ACTORS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            if (perso.system.stature.valour.value > vaillanceMaxValue) {
                vaillanceMaxValue = perso.system.stature.valour.value;
            }
            if (perso.system.stature.wisdom.value > sagesseMaxValue) {
                sagesseMaxValue = perso.system.stature.wisdom.value;
            }
        }

        // LOOP ON ALL ACTORS
        for (var i = 0; i < listeActors.length; i++) {
            let perso = listeActors[i];

            message.content = message.content + "<tr><td>" + perso.name + "</td>";
            if (perso.system.stature.valour.value == vaillanceMaxValue) {
                message.content = message.content + "<td style='color:darkblue; text-align:center; font-weight: bold'>" + perso.system.stature.valour.value + "</td>";
            } else {
                message.content = message.content + "<td style='text-align:center'>" + perso.system.stature.valour.value + "</td>";
            }
            if (perso.system.stature.wisdom.value == sagesseMaxValue) {
                message.content = message.content + "<td style='color:darkblue; text-align:center; font-weight: bold'>" + perso.system.stature.wisdom.value + "</td>";
            } else {
                message.content = message.content + "<td style='text-align:center'>" + perso.system.stature.wisdom.value + "</td>";
            }
            message.content = message.content + "</tr>";
        }

        // END OF THE HTML TEXT
        message.content = message.content + "</table>";

        // POST THE MESSAGE IN THE CHAT
        ChatMessage.create(message);
    }

    // ==================================================
    // REMET SANTE PHYSIQUE MAX DU TOKEN SELECTIONNE
    // ==================================================
    async characters_Set_EtatPhysiqueMax() {
        // ==================================================
        // - Endurance au max
        // - Fatigue de voyage à zéro
        // - Etat Epuisé mis à Non
        // - Etat Blessé mis à Non
        // - Etat Empoisonné mis à Non
        // ==================================================
        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_Set_EtatPhysiqueMax");

        let trad_selectTokenPj = macroUtil.getTraduction("select_token_pj_sagesse");
        let trad_actionTerminee = macroUtil.getTraduction("action_terminee");

        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn(trad_selectTokenPj);
        } else {
            console.log(token.actor);
            let monPJ = token.actor;
            let maxEndurance = monPJ.system.resources.endurance.max;

            const monUpdate = {
                data: {
                    resources: {
                        endurance: {
                            value: maxEndurance
                        },
                        travelFatigue: {
                            value: 0
                        }
                    },
                    stateOfHealth: {
                        poisoned: {
                            value: 0
                        },
                        weary: {
                            value: 0
                        },
                        wounded: {
                            value: 0
                        }
                    }
                }
            };
            const monUpdated = await monPJ.update(monUpdate);
            ui.notifications.info(trad_actionTerminee + monPJ.name);
        }
    }












    // ==================================================
    // AJOUT DE LA PROGRESSION EN FIN DE SEANCE
    // ==================================================
    async characters_Add_SessionProgression() {
        // Get traduction of terms
        let macroUtil = new MacroUtil("characters_Add_SessionExperience");

        let trad_DialogTitre = macroUtil.getTraduction("titre");
        let trad_DialogPtsAventure = macroUtil.getTraduction("trad_DialogPtsAventure");
        let trad_DialogPtsCompetence = macroUtil.getTraduction("trad_DialogPtsCompetence");
        let trad_DialogDestinataires = macroUtil.getTraduction("trad_DialogDestinataires");
        let trad_DialogTouslesjoueurs = macroUtil.getTraduction("trad_DialogTouslesjoueurs");
        let trad_DialogJoueurspresents = macroUtil.getTraduction("trad_DialogJoueurspresents");
        let trad_DialogContinuer = macroUtil.getTraduction("trad_DialogContinuer");
        let trad_DialogTouches = macroUtil.getTraduction("trad_DialogTouches");
        
        let trad_Progression = macroUtil.getTraduction("trad_Progression");
        let trad_Personnages = macroUtil.getTraduction("trad_Personnages");
        let trad_PtsAventure = macroUtil.getTraduction("trad_PtsAventure");
        let trad_PtsCompetences = macroUtil.getTraduction("trad_PtsCompetences");
        let nbPersos = 0;
        game.users.forEach(t => {
            if (t.character != null) {
                let perso = t.character;
                if (perso != null) {
                    if (perso.type == "character") {
                        nbPersos++;
                    }
                }
            }
        });
        
        let formHtml = "<table><tr><th colspan='2'><h2>"+trad_DialogTitre+"</h2></th></tr>";
        formHtml = formHtml + "<tr><th nowrap><label>"+trad_DialogPtsAventure+" : </label></th><td><input name='ptsAventure' style='width: 50px' value='2'/></td></tr>";
        formHtml = formHtml + "<tr><th nowrap><label>"+trad_DialogPtsCompetence+" : </label></th><td><input name='ptsCompetences' style='width: 50px' value='2'></td></tr>";
        formHtml = formHtml + "<tr><th nowrap><label>"+trad_DialogDestinataires+" : </label></th><td><select id='destinataires' multiple rows=15 style='width: 300px; height: " + (40+(nbPersos*20)) + "px;'>";
        formHtml = formHtml + "<option value='tous'>"+trad_DialogTouslesjoueurs+"</option>";
        formHtml = formHtml + "<option value='presents'>"+trad_DialogJoueurspresents+"</option>";
        game.users.forEach(t => {
            if (t.character != null) {
                let perso = t.character;
                if (perso != null) {
                    if (perso.type == "character") {
                        formHtml = formHtml + "<option value='" + t.name + "'>" + perso.name + " (" + t.name + ")</option>";
                    }
                }
            }
        });
        formHtml = formHtml + "</select></td></tr><tr><td></td><td>"+trad_DialogTouches+"</td></tr></table>";
        
        let hauteurFenetre = (nbPersos*21) + 270;
        new Dialog({
            title: trad_DialogTitre,
            content: formHtml,
            buttons: {
                yes: {
                    icon: "<i class='fas fa-check'></i>",
                    label: trad_DialogContinuer
                }
            },
            default: 'yes',
            close: html => {
                let ptsAventure = html.find('[name=ptsAventure]')[0]?.value || null;
                let ptsCompetences = html.find('[name=ptsCompetences]')[0]?.value || null;
                if (ptsAventure != null && ptsCompetences != null) {
                    let cible = html.find('select#destinataires');
                    var options = cible[0].selectedOptions;
                    var values = Array.from(options).map(({ value }) => value);
                    
                    let monMessage = "<table><tr><th>=== " + trad_Progression + " ===</th></tr>";
                    monMessage = monMessage + "<tr><td>+"+ ptsAventure+" " + trad_PtsAventure + "</td></tr>";
                    monMessage = monMessage + "<tr></td><td>+"+ ptsCompetences+" " + trad_PtsCompetences +"</td></tr>";
                    monMessage = monMessage + "<tr><th>=== " + trad_Personnages + " ===</th></tr>";
                    
                    if (values == "tous") {
                        game.users.forEach(t => {
                            if (t.character != null) {
                                let perso = t.character;
                                if (perso != null) {
                                    if (perso.type == "character") {
                                        this.ajoutePts(t.name, ptsAventure, ptsCompetences);
                                        monMessage = monMessage + "<tr><td>" + perso.name + "</td></tr>";
                                    }
                                }
                            }
                        });                        
                    } else if (values == "presents") {
                        game.users.forEach(t => {
                            if (t.active == true && t.character != null) {
                                let perso = t.character;
                                if (perso != null) {
                                    if (perso.type == "character") {
                                        this.ajoutePts(t.name, ptsAventure, ptsCompetences);
                                        monMessage = monMessage + "<tr><td>" + perso.name + "</td></tr>";
                                    }
                                }
                            }
                        });                        
                    } else {
                        for (var i = 0; i < values.length; i++) {
                            game.users.forEach(t => {
                                if (t.name === values[i]) {
                                    if (t.character != null) {
                                        let perso = t.character;
                                        if (perso != null) {
                                            if (perso.type == "character") {
                                                this.ajoutePts(t.name, ptsAventure, ptsCompetences);
                                                monMessage = monMessage + "<tr><td>" + perso.name + "</td></tr>";
                                            }
                                        }
                                    }
                                }
                            });
                        }
                    }
                    monMessage = monMessage + "</tr></table>"
                    let message = {
                        content: monMessage
                    };
                    ChatMessage.create(message);
                }
            }
        },{width:600, height:hauteurFenetre}).render(true);
    }


    async ajoutePts(nomPerso, ptsExperience, ptsProgression) {
        game.users.forEach(t => {
            if (t.name === nomPerso) {
                if (t.character != null) {
                    let perso = t.character;
                    if (perso != null) {
                        if (perso.type == "character") {
                            
                            // Points d'Experience
                            let curPtsExperience      = perso.system.stature.experience.value;
                            let curPtsExperienceTotal = perso.system.stature.experience.total.value;
                            let newPtsExperience      = parseInt(curPtsExperience,10) + parseInt(ptsExperience,10);
                            let newPtsExperienceTotal = parseInt(curPtsExperienceTotal,10) + parseInt(ptsExperience,10);

                            // Points de Progression
                            let curPtsProgression     = perso.system.advancementPoints.value;
                            let newPtsProgression     = parseInt(curPtsProgression,10) + parseInt(ptsProgression,10);

                            const monUpdate = {
                                data: {
                                    stature: {
                                        experience: {
                                            value: newPtsExperience,
                                            total: {
                                                value: newPtsExperienceTotal
                                            }
                                        }
                                    },
                                    advancementPoints: {
                                        value: newPtsProgression
                                    },
                                }
                            };
                            perso.update(monUpdate);
                        }
                    }
                }
            }
        });     
    }





































}