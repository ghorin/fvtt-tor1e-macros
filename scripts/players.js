export class MacroPlayers {

    constructor() {
    }

    listConnectedPlayers() {
        // ==================================================
        //    LISTE DES JOUEURS CONNECTES
        // ==================================================
        let message = {
            content: "<table><tr><th colspan=2 style='text-align:center'>LISTE DES JOUEURS</th></tr>",
            whisper: game.users.filter(u => u.isGM).map(u => u._id)
        };

        // For each active player (not the Gamemaster) with a player character
        game.users.forEach(t => {
            if (!t.isGM && t.active == true  && t.character != null) {
                let perso = t.character

                let nomJoueur = t.name;
                let nomPerso = perso.name;
                let roleJoueur = t.role;

                if (roleJoueur == 4) {
                    message.content = message.content + "<tr><td>" + nomJoueur + "</td><td>MJ</td></tr>";
                } else {
                    message.content = message.content + "<tr><td>" + nomJoueur + "</td><td>" + nomPerso + "</td></tr>";
                }
            }
        });

        message.content = message.content + "</table>";
        ChatMessage.create(message);
    }
}


