import { MacroUtil } from "./utils.js";

class myDialog extends Dialog {
    processWeapon( weaponName ) {
        game.tor1e.macro.utility.rollItemMacro(weaponName, 'weapon'),
        this.close();
    }

    activateListeners( html ) {
        super.activateListeners(html);
        html.find('.weaponList').click(ev => {
            const weaponName = $(ev.currentTarget).data("weapon-name");    
            this.processWeapon( weaponName );
        });
    }
}  

export class MacroCombat {

    constructor() {
    }

    // ==================================================
    // LANCE UNE ATTAQUE D'UN PERSONNAGE
    // ==================================================
    async combat_lanceAttaque() {
        // Get traduction of terms      
        let macroUtil = new MacroUtil("combat_lanceAttaque");

        let trad_selectToken = macroUtil.getTraduction("selectToken");
        let trad_titre = macroUtil.getTraduction("titre");
        let trad_choixArme = macroUtil.getTraduction("choixArme");

        let trad_boutonArme = macroUtil.getTraduction("arme");
        let trad_boutonRang = macroUtil.getTraduction("rang");
        let trad_boutonDegats = macroUtil.getTraduction("degats");
        let trad_boutonTaille = macroUtil.getTraduction("taille");
        let trad_boutonBlessure = macroUtil.getTraduction("blessure");

        let trad_haches = macroUtil.getTraduction("haches");
        let trad_arcs = macroUtil.getTraduction("arcs");
        let trad_lances = macroUtil.getTraduction("lances");
        let trad_epees = macroUtil.getTraduction("epees");


        if (game.tor1eMacros.TOR1E_MACROS_DEBUG) {
            console.log('====================================================');
            console.log('========= TOR1E MACROS : ATTACK DEBUG MODE =========');
            console.log('====================================================');
            console.log("trad_haches=" + trad_haches);
            console.log("trad_arcs=" + trad_arcs);
            console.log("trad_lances=" + trad_lances);
            console.log("trad_epees=" + trad_epees);
        }

        // RECUPERER LE TOKEN
        let tokens = canvas.tokens.controlled;
        let token = null;
        if (tokens.length > 0) {
            token = tokens[0];
        }
        if (token == null) {
            ui.notifications.warn(trad_selectToken);
        } else {
            // PERSONNAGE
            let monPerso = token.actor;

            // FORM
            let pas = 0;
            let form = `<div style="display: inline-block;"><h3>`+trad_choixArme+`</h3></div>`;
            form = form + `<table>`;
            form = form + `<tr style='text-align:center'><th></th><th>`+trad_boutonArme+`</th><th>`+trad_boutonRang+`</th><th>`+trad_boutonDegats+`</th><th>`+trad_boutonTaille+`</th><th>`+trad_boutonBlessure+`</th></tr>`;

            if (monPerso.type === "adversary") {
                // CAS 1 : ADVERSAIRE
                monPerso.items.forEach(a => {
                    if (a.type == "weapon") {
                        form = form + `<tr style='text-align:center'>`;
                        form = form + `<td><input type="image" src="` + a.img + `" width="64px" height="64px" class="weaponList" data-weapon-name="${a.name}" border="0px" id="weapon${pas}"></input></td>`;
                        form = form + `<td style='text-align:left'>&nbsp;` + a.name + `</td>`;
                        form = form + `<td>` + a.system.skill.value + `</td>`;
                        form = form + `<td>` + a.system.damage.value + `</td>`;
                        form = form + `<td>` + a.system.edge.value + `</td>`;
                        form = form + `<td>` + a.system.injury.value + `</td>`;
                        form = form + `</tr>`;
                        pas = pas + 1;
                    }
                });
            } else if (monPerso.type === "character") {
                // CAS 2 : PJ
                monPerso.items.forEach(a => {
                    if (a.type == "weapon") {
                        let nomCompetence = a.system.skill.name;
                        let nomCompetenceCulturelle = "";
                        let groupeCompetence = a.system.group.value;
                        let equipee = a.system.equipped.value;
                        let nomCompetenceUtilisee = nomCompetence;
                        if (equipee == true) {
                            if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("========== " + a.name + " ==========");
                            if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("Data : \n..... Skill=" + nomCompetence + "\n..... Group=" + groupeCompetence);
                            if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("Weapon skill ?");

                            let rangCompetence = -1;
                            monPerso.items.forEach(c => {
                                // Competence avec nom = nom de l'arme
                                if (c.type == "skill" && c.name == nomCompetence) {
                                    rangCompetence = c.system.value;
                                    if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("..... Weapon skill " + c.name + " found with rank " + rangCompetence);
                                }
                            });
                            if (rangCompetence == -1) {
                                if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... Weapon skill not found")
                                if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("Cultural weapon skill ?");
                                let nomCompetenceCulturelle = "none";
                                if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... groupeCompetence=<" + groupeCompetence + ">");
                                if (game.tor1eMacros.TOR1E_MACROS_DEBUG) {
                                    let rechAxes = groupeCompetence.indexOf("axes");
                                    let rechArcs = groupeCompetence.indexOf("bows");
                                    let rechLances = groupeCompetence.indexOf("spears");
                                    let rechEpees = groupeCompetence.indexOf("swords");
                                    console.log("... search for axe=" + rechAxes);
                                    console.log("... search for bows=" + rechArcs);
                                    console.log("... search for spears=" + rechLances);
                                    console.log("... search for swords=" + rechEpees);
                                }
                                if (groupeCompetence.indexOf("axes") != -1) {
                                    if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... found : hache");
                                    nomCompetenceCulturelle = trad_haches;
                                } else if (groupeCompetence.indexOf("bows") != -1) {
                                    if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... found : arc");
                                    nomCompetenceCulturelle = trad_arcs;
                                } else if (groupeCompetence.indexOf("spears") != -1) {
                                    if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... found : lance");
                                    nomCompetenceCulturelle = trad_lances;
                                } else if (groupeCompetence.indexOf("swords") != -1) {
                                    if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... found : épée");
                                    nomCompetenceCulturelle = trad_epees;
                                } else {
                                    if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... Cultural weapon skill not found");
                                }
                                monPerso.items.forEach(c => {
                                    // Competence culturelle avec nom = (groupe de l'arme)
                                    if (c.type == "skill" && c.name == nomCompetenceCulturelle) {
                                        rangCompetence = c.system.value;
                                        nomCompetenceUtilisee = nomCompetenceCulturelle;
                                        if (game.tor1eMacros.TOR1E_MACROS_DEBUG) console.log("... Cultural weapon skill " + nomCompetenceCulturelle + " found with rank " + rangCompetence);
                                    }
                                });
                            }

                            form = form + `<tr style='text-align:center'>`;
                            form = form + `<td><input type="image" src="` + a.img + `" width="64px" height="64px" class="weaponList" data-weapon-name="${a.name}" border="0px" id="weapon${pas}"></input></td>`;
                            form = form + `<td style='text-align:left'>` + a.name + `</td>`;
                            form = form + `<td>` + rangCompetence + `</td>`;
                            form = form + `<td>` + a.system.damage.value + `</td>`;
                            form = form + `<td>` + a.system.edge.value + `</td>`;
                            form = form + `<td>` + a.system.injury.value + `</td>`;
                            form = form + `</tr>`;
                            pas = pas + 1;
                        }
                    }
                });
            }
            // FIN DU FORM
            form = form + `</table>`;

            // AFFICHER LA BOITE DE DIALOGUE DE CHOIX DE L'ARME
            let data = {
                title: trad_titre + " " + monPerso.name,
                content: form,
                buttons: {
                    //    use: {
                    //        label: "Fermer",
                    //        callback: html => close()
                    //    }
                },
                default: "Fermer",
                close: () => { }
            };
            const options = { width: 600, height: 100 + (pas * 90) };
            let mydiag = new myDialog(data, options);
            mydiag.render(true);
        }
    }
}