# FVTT-TOR1e-Macros
**DESCRIPTION**

This module contains JavaScript function that may be used for adding functionalities for the Loremaster and/or players of The One Ring 1e.


**CREDITS**

Few icons used in this module are creation from <a href="https://game-icons.net/">game-icons.net</a> and used under <a href="https://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a> LICENCE.


**INSTALLATION**

Install from FoundryVTT setup / Add-on modules / Install module with Manifest URl : https://gitlab.com/ghorin/fvtt-tor1e-macros/-/raw/master/module.json


**HOW TO USE**

All the Macros provided by this module are available inside macro compendiums for french and english (more languages to come).
You can open those compendiums and drag&drop the macros into your Hotbar.
You can also create manually a "script" macro in your Hotbar and put the corresponding JS code as in below table.

All icons provided are in folder modules/fvtt-tor1e-macros/icons

| Group |Icon | Macro | Description | Video |
| ------ | ------ | ------ | ------ | ------ |
| **Characters** | [character_sheet.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/character_sheet.webp) | `game.tor1eMacros.sheet_DisplaySheet();` | Display the player sheet (Player) or the selected token sheet (GM) | |
| | [art_work.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/art_work.webp) | `game.tor1eMacros.sheet_DisplayTokenImage();` | Display the art work of the selected token to all players | [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Show-token-image-to-players.mp4) |
| | [health_physical.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/health_physical.webp) | `game.tor1eMacros.characters_List_Health_Physical();` | Get a summary of the physical health of all players characters  | [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Health-synthesis.mp4) |
| | [health_moral.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/health_moral.webp) | `game.tor1eMacros.characters_List_Health_Moral();` | Get a summary of the moral health of all players characters | [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Moral-synthesis.mp4) |
| | [valour_wisdom.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/valour_wisdom.webp) | `game.tor1eMacros.characters_List_Valour_Wisdom();` | Get the Valour and Wisdom values of each character + emphasis on the max values.| [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Valour-Wisdom.mp4) |
| | [health_max.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/health_max.webp) | `game.tor1eMacros.characters_Set_EtatPhysiqueMax();` | Put back the max health values for the selection character | [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Reset-Endurance-Fatigue.mp4) |
| | [icons/skills/movement/arrows-up-trio-red.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/health_max.webp) | `game.tor1eMacros.characters_List_Experience_Courage();` | Calculate the Courage points for each character | [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Courage-Points.mp4) |
| | [advancement_points.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/advancement_points.webp) | `game.tor1eMacros.characters_List_Advancement_Points();` | List the Advancement Points per skills groups for each character | |
| | []| `game.tor1eMacros.characters_Add_SessionProgression();` | Add Experience and Advancement points at end of session | |
||||||
| **Combat** | [attack.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/attack.webp) | `game.tor1eMacros.combat_lanceAttaque();` | List the weapons, choose one and attack | |
||||||
| **Tokens** | [token_mort.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/token_mort.webp) | `game.tor1eMacros.tokens_Set_DeadOrNot();` | Put a red cross over the token to indicate it is dead | |
| | [etat-epuise.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/etat-epuise.webp) | `game.tor1eMacros.tokens_Set_EtatEpuise();` | Put a small icon indicating that the character is Weary in the top left of the token | |
| | [etat-blesse.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/etat-blesse.webp) | `game.tor1eMacros.tokens_Set_EtatBlesse();` | Put a small icon indicating that the character is injured in the top left of the token | |
| | [etat-inconscient.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/etat-inconscient.webp) | `game.tor1eMacros.tokens_Set_EtatInconscient();` | Put a small icon indicating that the character is inconscious or out-of combat in the top left of the token | |
| | [etat-mort.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/etat-mort.webp) | `game.tor1eMacros.tokens_Set_EtatMort();` | Put a small icon indicating that the character is dead in the top left of the token | |
| | [invisible.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/invisible.webp) | `game.tor1eMacros.objects_Set_Hidden_or_not();` | Hide or unhide tokens or tiles| [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Hide-unhide.mp4) |
| | [token_resize.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/token_resize.webp) | `game.tor1eMacros.tokens_Set_Size();` | Resize tokens images| [Demo](http://ostolinde.free.fr/anneau_unique/foundryvtt/FVTT-TOR1E-MACROS-Tokens-resize.mp4) |
||||||
| **Chat** | [message.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/message.webp) | `game.tor1eMacros.chat_SendMessage();` | Send a message to the chat of one or several loremaster/players | |
| | [chat_bubble.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/chat_bubble.webp) | `game.tor1eMacros.chat_SendMessageBuble();` | Send a message in a dialog bubble on top of a token | |
||||||
| **Joueurs** | [players.webp](https://gitlab.com/ghorin/fvtt-tor1e-macros/-/blob/master/icons/players.webp) | `game.tor1eMacros.players_ListConnectedPlayers();` | List the players with their attached character | |



 
